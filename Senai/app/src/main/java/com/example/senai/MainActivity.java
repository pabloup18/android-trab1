package com.example.senai;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText edtTextTexto = findViewById(R.id.Et_texto);
        final EditText edtTextTexto2 = findViewById(R.id.Et_texto2);
        Button btn_mostrar = findViewById(R.id.btn_mostrar);
        Button btn_apagar = findViewById(R.id.btn_apagar);
        btn_mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String final_msg = edtTextTexto.getText() + " " + edtTextTexto2.getText();
                Toast.makeText(MainActivity.this, final_msg, Toast.LENGTH_LONG).show();
            }
        });

        btn_apagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTextTexto.setText("");
                edtTextTexto2.setText("");
            }
        });
    }
}